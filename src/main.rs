use serde::{Deserialize, Serialize};
use serde_json::Result;
use std::fs;
use std::path::Path;

#[derive(Serialize, Deserialize)]
struct Person {
    name: String,
    age: u8,
    phones: Vec<String>,
}

/// Example deserialization into a type
fn typed_example() -> Result<()> {
    // Some JSON input data as a &str. Maybe this comes from the user.
    let data = r#"
        {
            "name": "John Doe",
            "age": 43,
            "phones": [
                "+44 1234567",
                "+44 2345678"
            ]
        }"#;

    // Parse the string of data into a Person object. This is exactly the
    // same function as the one that produced serde_json::Value above, but
    // now we are asking it for a Person as output.
    let p: Person = serde_json::from_str(data)?;

    // Do things just like with any other Rust data structure.
    println!("Please call {} at the number {}", p.name, p.phones[0]);

    Ok(())
}

#[derive(Serialize, Deserialize)]
struct Ad {
    strName: String,
    strDesc: String
}

/// Example deserialization into a type; see ads.json
fn revive_ads() -> Result<()> {
    // Some JSON input data as a &str. Maybe this comes from the user.
    let data = r#"
    [
        {   "strName" : "Oneirotix Dalisay",
            "strDesc" : "She's Everything To You\nDalisay is the Dream Girl you can have - Experience One Romantic Night with Dalisay in the Terrace Gardens of Hangzhou. Can you Woo the Dazzling Actress before the Stroke of Midnight? Try the new dream from Oneirotix today."
        },
        {   "strName" : "The Abyss: Personality Matrices Of Notorious Psychopaths",
            "strDesc" : "Subscribe To The Abyss: Personality Matrices Of Notorious Psychopaths. This Month's Installation: Become Ayaka Ito, The Serial Cannibal Who Ate Her Victims Alive..."
        },
        {   "strName" : "Chan Rak Khun",
            "strDesc" : "Don't Get Scammed!! Foreigners To Calico City Require A Good Hearted Citizen To Be Their Guide, BUT!!!! Don't Pay Just Any Young Smiling Man On The Wharf -- Scammers Abound!! Enter Chan Rak Khun, To Offer Guide Services At FIXED PRICES With Your Safety OUR GUARANTEE!!! Chan Rak Khun -- It's Better To Be Safe Than Sorry!!"
        }
    ]"#;

    // Parse the string of data into a Person object. This is exactly the
    // same function as the one that produced serde_json::Value above, but
    // now we are asking it for a Person as output.
    let p: Vec<Ad> = serde_json::from_str(data)?;

    // Do things just like with any other Rust data structure.
    println!("Consume {}! {}", p[0].strName, p[0].strDesc);

    Ok(())
}

/// Example deserialization into a type; see ads.json
fn revive_ads_json(ad_path: &Path) -> Option<Vec<Ad>> {
    // // Some JSON input data as a &str. Maybe this comes from the user.
    // let data = r#"
    // [
    //     {   "strName" : "Oneirotix Dalisay",
    //         "strDesc" : "She's Everything To You\nDalisay is the Dream Girl you can have - Experience One Romantic Night with Dalisay in the Terrace Gardens of Hangzhou. Can you Woo the Dazzling Actress before the Stroke of Midnight? Try the new dream from Oneirotix today."
    //     },
    //     {   "strName" : "The Abyss: Personality Matrices Of Notorious Psychopaths",
    //         "strDesc" : "Subscribe To The Abyss: Personality Matrices Of Notorious Psychopaths. This Month's Installation: Become Ayaka Ito, The Serial Cannibal Who Ate Her Victims Alive..."
    //     },
    //     {   "strName" : "Chan Rak Khun",
    //         "strDesc" : "Don't Get Scammed!! Foreigners To Calico City Require A Good Hearted Citizen To Be Their Guide, BUT!!!! Don't Pay Just Any Young Smiling Man On The Wharf -- Scammers Abound!! Enter Chan Rak Khun, To Offer Guide Services At FIXED PRICES With Your Safety OUR GUARANTEE!!! Chan Rak Khun -- It's Better To Be Safe Than Sorry!!"
    //     }
    // ]"#;

    if ad_path.is_file() {
        // Expected case

        // Read JSON data from file
        let data = fs::read_to_string(ad_path).expect("Unable to read ads.json");

        // Parse the string of data into a Person object. This is exactly the
        // same function as the one that produced serde_json::Value above, but
        // now we are asking it for a Person as output.
        let p: Vec<Ad> = match serde_json::from_str(data.as_str()) {
            Ok( m ) => { m }
            Err( _f ) => { return None }
        };

        // Do things just like with any other Rust data structure.
        println!("Consume {}! {}", p[2].strName, p[2].strDesc);

        Some(p)
    } else {
        None
    }

}

// #[derive(Serialize, Deserialize)]
// struct Loot {
//     strName : String,
//     aCOs    : Vec<String>,
//     aLoots  : Vec<String>
//     strType : String
// }

// /// Example deserialization into a type; see loot.json
// fn revive_loot_json(loot_path: &Path) -> Option<Vec<Loot>> {
//     if loot_path.is_file() {
//         // Expected case

//         // Read JSON data from file
//         let data = fs::read_to_string(loot_path).expect("Unable to read loot.json");

//         let p: Vec<Loot> = match serde_json::from_str(data.as_str()) {
//             Ok( m ) => { m }
//             Err( _f ) => { return None }
//         };

//         // Do things just like with any other Rust data structure.
//         println!("Loot {} is a(n) {}", p[0].strName, p[0].strType);

//         Some(p)
//     } else {
//         println!("Cannot file file {}", loot_path.display());
//         None
//     }
// }

#[derive(Serialize, Deserialize)]
struct SaveInfo {
    strName : String,
    playerName : String,
    shipName   : String,
    saveNote   : String,
    formerOccupation : String,
    version          : String,
    age              : f64,
    money            : f64,
    playTimeElapsed  : f64,
    simTimeElapsed   : f64,
    simTimeCurrent   : f64,
    realWorldTime    : String
}

/// Example deserialization into a type; see saveInfo.json
fn revive_saveinfo_json(save_path: &Path) -> Option<Vec<SaveInfo>> {
    // None
    if save_path.is_file() {
        // Expected case

        // Read JSON data from file
        let data = fs::read_to_string(save_path).expect("Unable to read saveInfo.json");

        // Parse the string of data into a Person object. This is exactly the
        // same function as the one that produced serde_json::Value above, but
        // now we are asking it for a Person as output.
        let p: Vec<SaveInfo> = match serde_json::from_str(data.as_str()) {
            Ok( m ) => { m }
            Err( _f ) => { return None }
        };

        // Do things just like with any other Rust data structure.
        println!("Captain {} of (the) {}", p[0].playerName, p[0].shipName);

        Some(p)
    } else {
        None
    }
}

/// Driver routine
fn main() {

    // println!("Hello, world!");

    // match typed_example() {
    //     Ok( () ) => { println!("Found a Person") }
    //     Err( _f ) => { println!("Cannot unpack data as Person") }
    // }

    // let assetdata_path = Path::new(r"/home/apthorpe/dp/git-proj/ostraspect/data/StreamingAssets/data");
    let assetdata_path = Path::new(r"C:\Users\apthorpe\Documents\git_projects\ostraspect\data\StreamingAssets\data");
    let ad_path = assetdata_path.join("ads.json");
    // match revive_ads() {
    //     Ok( () ) => { println!("Found a pile of Ads") }
    //     Err( _f ) => { println!("Cannot unpack data as Vec<Ad>") }
    // }

    let ads : Vec<Ad> = match revive_ads_json(&ad_path) {
        Some(m) => { m }
        None => { panic!("No ads found") }
    };

    // let loot_path = assetdata_path.join("loot.json");
    // let loot : Vec<Loot> = match revive_loot_json(&loot_path) {
    //     Some(m) => { m }
    //     None => { println!("No loot parsable from {}", loot_path.display()); vec!() }
    // };

    // let savedata_path = Path::new(r"/home/apthorpe/dp/git-proj/ostraspect/data/Save1");
    let savedata_path = Path::new(r"C:\Users\apthorpe\Documents\git_projects\ostraspect\data\Save1");
    let saveinfo_path = savedata_path.join("saveInfo.json");
    // match revive_saveinfo() {
    //     Ok( () ) => { println!("Found save information") }
    //     Err( _f ) => { println!("Cannot unpack data as Vec<saveInfo>") }
    // }

    let saveinfo : Vec<SaveInfo> = match revive_saveinfo_json(&saveinfo_path) {
        Some(m) => { m }
        None => { panic!("No saveinfo found") }
    };
}
