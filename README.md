# Ostraspect

## Introduction

Ostraspect is ...

## Background and Operation

## Usage

```bat
ostraspec v0.1: JSON asset explorer for Ostranauts
Usage: Debug\ostraspec.exe [options] FILENAME

Options:
    -h, --help          Print this help menu
    -v, --verbose       Show more detailed log info
    -V, --version       Print version info and exit
```

`FILENAME` is an Ostranauts game asset file in JSON format.

## Known issues

None known at present.

## Project Contributions

Contributions, bug reports, and feature requests are welcome! See [Ostraspect's gitlab issue tracker](https://gitlab.com/apthorpe/ostraspect/-/issues) for details. Please review `CODE_OF_CONDUCT.md`. `tl;dr` treat others with respect and dignity.

## Copyright

The Ostrspect project is copyright 2022, Bob Apthorpe.

### License

The Ostraspect source code and documentation are released under an MIT license located in the file `LICENSE`.

